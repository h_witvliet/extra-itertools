from collections import deque
from itertools import islice, tee, izip


class Iterbuffer(object):
    '''class to act as a buffer for iterators. You can look forwards and
    backwarks. A typical example looks like:
        mybuf2 = iterbuffer(iter(xrange(10)), size=15)
        print 'start:', mybuf2.get_offset(-1), mybuf2.get_offset(1)
        print('-'*78)
        a = (2*x for x in mybuf2 if x % 2 == 1)
        for i in a:
            print i, mybuf2.get_offset(-1), mybuf2.get_offset(0), \
                    mybuf2.get_offset(1)
        '''

    def __init__(self, iterable, size=10, empty=None):
        '''iterable gets buffered with a max size (forward and backwards)
        elements. empty contains the value that will be returned if if looking
        forwards or backwards cannot find the item. Be careful with mutable
        objects.'''
        self.iterable = iterable
        self.empty = empty
        # backview[0] gives the yielded element, backview[1] the previously
        # yielded element, etc.
        self.backview = deque(maxlen=size)
        # preview[0] holds the element to be yielded, etc.
        # if the list is smaller than the buffer size, only these elements gets
        # buffered
        self.preview = deque(islice(iterable, size), maxlen=size)

    def __iter__(self):
        '''Make sure we behave as an iterable'''
        return self

    def get_offset(self, offset):
        '''return the element at the offset position relative to the current
        element. negative is elements not yet served'''
        try:
            if offset < 0:
                return self.preview[-offset - 1]
            else:
                return self.backview[offset]
        except IndexError:
            return self.empty

    def __next__(self):
        # get next element from preview buffer
        # if there is no element, we get an IndexError
        try:
            elt = self.preview.popleft()
        except IndexError:
            raise StopIteration

        #shift both buffers (if possible)
        self.backview.appendleft(elt)
        try:
            newpreviewelt = next(self.iterable)
            self.preview.append(newpreviewelt)
        except StopIteration:
            pass
        return elt

    def next(self):
        '''We now should work on 2.7 and 3.x'''
        return self.__next__()


class itercounter(object):
    """helper class to keep track of the number of elements an iterator has
    yielded. The attribute count gives this number."""

    def __init__(self, iterable):
        self.count = 0
        self.iterable = iterable

    def __next__(self):
        elt = next(self.iterable)
        self.count += 1
        return elt

    def next(self):
        return self.__next__()

    def __iter__(self):
        return self


# from the Python documentation
def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return izip(a, b)


# and something similar for 3 elements at a time
def triple(iterable):
    #s -> (s0,s1,s2), (s1,s2,s3), (s2, s3,s3), ..."
    a, b, c = tee(iterable, 3)
    next(b, None)
    next(c, None)
    next(c, None)
    return izip(a, b, c)
